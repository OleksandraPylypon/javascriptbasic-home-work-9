//створюємо масив
const array = ["names", 42, true, 12, 8, 15];
const parentElement = document.getElementById("my-list-container");
//створюємо функцію, що приймає на вхід два аргементи:
function showList(array, parent = document.body) {
  //створюємо список за допомогою <ul>
  const myList = document.createElement("ul");

  //створюємо елементи списку <li>
  for (let i = 0; i < array.length; i++) {
    const listItem = document.createElement("li");
    listItem.innerText = array[i];
    myList.appendChild(listItem);
  }
  parent.appendChild(myList);
}

showList(array, parentElement);
